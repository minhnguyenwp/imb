/**
 * Media 767px : commandes
 */
@media (max-width: 767px) {
    .associes-list {
        .product-item {
            padding-left: 6px;
            padding-right: 6px;
        }
    }
    .option-address-wrap {
        .col-md-6 {
            margin-bottom: 30px;
            &:last-child {
                margin-bottom: 0;
            }
        }
    }
    #modalDeclaration, #modalDeclarationStep2 {
        .inner {
            padding-left: 40px;
            padding-right: 40px;
        }
    }
    .commandes-search {
        .container {
            justify-content: flex-start;
        }
    }
    .commandes-wrap {
        .table-responsive {
            margin-bottom: 60px;
            padding-bottom: 0;
            // .table-imb {
            //     width: 900px;
            // }
        }
    }
    .detail-command-info {
        .inner {
            padding: 20px;
        }
        .col-md-4 {
            margin-bottom: 20px;
            &:last-child {
                margin-bottom: 0;
            }
        }
    }
    .table-payment {
        td {
            &.empty {
                display: none;
            }
            &.price {
                width: 120px;
                text-align: right;
            }
        }
    }
    .detail-button-wrap {
        flex-direction: column;
        .btn {
            margin-right: 0;
            &:first-child {
                margin-bottom: 20px;
            }
        }
    }
    .block-title-page {
        .block-title-large {
            left: -15px;
        }
    }
    .pb-60 {
        padding-bottom: 40px;
    }
    .mes-address-wrap {
        margin-bottom: 60px;
    }
    .mes-information {
        .mes-right {
            padding-left: 15px;
        }
    }
    .mes-address-title {
        .col-right {
            margin-top: 10px;
            justify-content: flex-start;
        }
    }
    .mes-form-wrap {
        .inner-form {
            margin-bottom: 30px;
            &::after {
                display: none;
            }
        }
        .form-group {
            .col-md-6 {
                margin-bottom: 20px;
                &:last-child {
                    margin-bottom: 0;
                }
            }
        }
    }
    .gestion-list {
        .gestion-item {
            flex: 0 0 50%;
            max-width: 50%;
        }
    }
    .mes-address-title {
        margin-bottom: 20px;
    }
    .step-item {
        flex: 0 0 33.3333333333%;
        max-width: 33.3333333333%;
        a {
            justify-content: flex-start;
        }
        &:first-child {
            a {
                padding-left: 20px;
            }
        }
        &:last-child {
            justify-content: flex-end;
            a {
                padding-right: 20px;
            }
        }
    }
    .term-inner {
        .col-md-6 {
            margin-bottom: 15px;
            &:last-child {
                margin-bottom: 0;
            }
        }
    }
    .block-options {
        margin-bottom: 60px;
    }
    .method-wrap {
        width: 100%;
    }
    .option-item {
        .opt-pic {
            width: 60px;
        }
    }
    .card-box {
        .card-title {
            a {
                opacity: 1;
                visibility: visible;
                transform: scale(1);
                z-index: 1;
            }
        }
    }
}
@media (max-width: 660px) {
    .mes-information {
        .modifier {
            position: relative;
            top: auto;
            right: auto;
            display: block;
            margin-bottom: 20px;
            text-align: right;
        }
        .mes-icon {
            width: 80px;
            height: 80px;
            flex: 0 0 80px;
            .ico {
                width: 40px;
                height: 34px;
                -webkit-mask-size: cover;
                -moz-mask-size: cover;
                mask-size: cover;
            }
        }
    }
}
@media (max-width: 640px) {
    .link_mod_search {
        display: flex;
        align-items: center;
        justify-content: center;
        width: 30px;
        height: 30px;
        .ico {
            transition: none;
            background-color: #d9d9d9;
        }
    }
    .commandes-search {
        .container {
            position: relative;
            flex-direction: column-reverse;
        }
        .block-search-wrap {
            display: none;
            position: absolute;
            background-color: #f6f6f6;
            padding: 5px;
            top: 100%;
            left: 30px;
            z-index: 10;
            border: none;
            box-shadow: 0 1px 5px rgba(0,0,0,0.2);
            &.shw {
                display: block;
            }
            &::before {
                content: '';
                position: absolute;
                z-index: 1;
                top: -10px;
                left: 5px;
                width: 0;
                height: 0;
                border-style: solid;
                border-width: 0 10px 10px 10px;
                border-color: transparent transparent #f6f6f6 transparent;
            }
        }
        .commandes-menu-left {
            margin-left: 0;
            padding-left: 0;
            border-left: none;
            margin-bottom: 10px;
        }
        &.sav-search {
            padding-bottom: 0;
        }
    }
    .commandes-menu-link {
        width: 100px;
    }
    .choose-date {
        align-items: flex-start;
        flex-direction: column;
        .desc {
            text-align: left;
        }
        .inner {
            margin-bottom: 15px;
        }
    }
    .calendar-wrap {
        width: 250px;
    }
}
@media (max-width: 480px) {
    .gestion-list {
        .gestion-item {
            flex: 0 0 100%;
            max-width: 100%;
        }
    }
    .option-item {
        .opt-title {
            font-size: 12px;
            line-height: 15px;
        }
        .txt {
            font-size: 12px;
            line-height: 14px;
        }
        .date {
            font-size: 13px;
            line-height: 17px;
        }
        .opt-content {
            .opt-price {
                font-size: 12px;
                line-height: 14px;
            }
        }
    }
    .panier-promo {
        .promo-input-wrap {
            width: 100%;
        }
        .promo-ttl {
            font-size: 14px;
            line-height: 17px;
        }
    }
    .card-box {
        .card-ttl {
            font-size: 13px;
        }
    }
    .method-wrap {
        .method-ttl {
            font-size: 14px;
            line-height: 18px;
        }
    }
    .choose-date {
        .date-ttl {
            font-size: 14px;
            line-height: 18px;
        }
    }
    .checkout-term {
        .ipt_checkbox + label {
            font-size: 12px;
            line-height: 16px;
        }
    }
    .commandes-search .block-search-wrap {
        width: 250px;
    }
    .table-mobile {
        .ctent, .title {
            font-size: 12px;
        }
        .table-item {
            font-size: 12px;
            padding: 10px;
            .text {
                font-size: 11px;
            }
            .col-name {
                .desc, .ttl {
                    font-size: 12px;
                    line-height: 16px;
                }
            }
            .clor-item {
                font-size: 12px;
                line-height: 16px;
                .size {
                    font-size: 12px;
                }
            }
        }
        .col-id {
            flex: 0 0 35%;
            max-width: 35%;
        }
        .col-name {
            flex: 0 0 65%;
            max-width: 65%;
        }
        // .col-6 {
        //     &:last-child {
        //         text-align: right;
        //     }
        // }
        .table-item {
            .action-circle {
                &.edit {
                    top: 17px;
                }
                &.del {
                    top: -15px;
                }
            }
        }
        .custom-number {
            float: right;
        }
    }
    .table-detail-commandes {
        .table-mobile {
            .table-item {
                padding-right: 0;
                .clor-item {
                    font-size: 11px;
                    line-height: 14px;
                    .size {
                        font-size: 11px;
                    }
                }
            }
        }
    }
}
@media (max-width: 450px) {
    .option-item {
        .label-content {
            width: 100%;
            align-items: center;
            flex-direction: column;
        }
        .opt-content {
            align-items: center;
            flex-direction: column;
            text-align: center;
            .ctent {
                width: 100%;
            }
            .opt-price {
                margin-top: 10px;
                justify-content: center;
                width: 100%;
            }
        }
        .opt-pic {
            margin-right: 0;
            margin-bottom: 10px;
        }
        .insert-file {
            justify-content: center;
        }
    }
    .options-button {
        text-align: center;
    }
}
@media (max-width: 380px) {
    .checkout-step {
        .container {
            &::after {
                right: 45px;
            }
        }
    }
    .panier-promo {
        .btn-promo {
            font-size: 12px;
            padding: 0 10px;
        }
    }
    .term-method {
        align-items: center;
    }
    .mes-information {
        .mes-inner {
            align-items: flex-start;
            flex-direction: column;
        }
        .mes-icon {
            margin-right: 0;
            margin-bottom: 20px;
        }
    }
    .btn.btn-orange.lrg, .btn.btn-dark.lrg {
        padding-left: 20px;
        padding-right: 20px;
    }
    .group-button.has-two .btn {
        margin-right: 0;
        display: block;
        width: 100%;
        margin-bottom: 15px;
        &:last-child {
            margin-bottom: 0;
        }
    }
    .mess-item {
        flex-direction: column;
        .ctent {
            flex: 0 0 100%;
            width: 100%;
        }
        &.date {
            align-items: center;
        }
        &.right {
            flex-direction: column-reverse;
            align-items: flex-end;
            .avt {
                margin-left: 0;
                margin-bottom: 10px;
            }
        }
        &.left {
            .avt {
                margin-right: 0;
                margin-bottom: 10px;
            }
        }
        .ctent {
            .text {
                padding: 10px 20px;
                font-size: 14px;
                line-height: 18px;
            }
        }
    }
    .table-product-wrap {
        .prod-info {
            font-size: 14px;
            line-height: 18px;
        }
    }
    .method-wrap {
        .method-list {
            flex-direction: column;
            align-items: flex-start;
        }
        .method-item {
            margin-right: 0px;
            margin-bottom: 10px;
        }
        .txt {
            font-size: 12px;
            line-height: 16px;
        }
    }
}
@media (max-width: 360px) {
    .checkout-step {
        .container {
            &::after {
                right: 40px;
            }
        }
    }
}