(function( $ ) { 
/**
 * FORM - ONLOAD - JS
 * -------------
 * 1. Form Validate Error
 * 2. Form Validate Success
 */
/* ----------------------------------------------- */
/* ------------- FrontEnd Functions -------------- */
/* ----------------------------------------------- */
/**
 * 1. Form Validate Error
 */
function formValidateError() {
    if(!$('.btn-contact').length) { return; }

    $('.btn-contact').on('click', function(e) {
        e.preventDefault();

        toastr.options.progressBar = true;
        toastr.options.closeButton = true;
        toastr.options.closeHtml = '<button><i class="ico close"></i></button>';
        toastr.options.closeMethod = 'fadeOut';
        toastr.options.closeDuration = 500;
        toastr.options.closeEasing = 'swing';
        toastr.options.timeOut = 5000;
        toastr.options.preventDuplicates = true;
        toastr.options.positionClass = "toast-bottom-right";

        // message
        toastr.error('Merci de renseigner les champs manquants');
    });
}
/**
 * 2. Form Validate Success
 */
function formValidateSuccess() {
    if(!$('.btn-login').length) { return; }

    $('.btn-login').on('click', function(e) {
        e.preventDefault();

        toastr.options.progressBar = true;
        toastr.options.closeButton = true;
        toastr.options.closeHtml = '<button><i class="ico close"></i></button>';
        toastr.options.closeMethod = 'fadeOut';
        toastr.options.closeDuration = 500;
        toastr.options.closeEasing = 'swing';
        toastr.options.timeOut = 5000;
        toastr.options.preventDuplicates = true;
        toastr.options.positionClass = "toast-bottom-right";

        // message
        toastr.success('Mis à jour avec succés');
    });
}
/* ----------------------------------------------- */
/* ----------------------------------------------- */
/* OnLoad Page */
$(document).ready(function($){
    // 1.
    formValidateError();
    // 2.
    formValidateSuccess();
});
/* OnLoad Window */
var init = function () {   

};
window.onload = init;
})(jQuery);